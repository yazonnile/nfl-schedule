import * as path from 'path';

export const getFilePath = (fileName: string, folder = process.cwd()): string => {
  return path.join(folder, fileName);
};
