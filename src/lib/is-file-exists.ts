import * as fs from 'fs';
import {getFilePath} from './get-file-path';

export const isFileExists = ({
  fileName,
  folder,
}: {
  fileName: string;
  folder?: string;
}): boolean => {
  try {
    fs.accessSync(getFilePath(fileName, folder), fs.constants.R_OK | fs.constants.W_OK);

    return true;
  } catch (err) {
    return false;
  }
};
