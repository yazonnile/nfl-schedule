import * as fs from 'fs';
import {getFilePath} from './get-file-path';

export const openFile = ({
  fileName,
  folder,
}: {
  fileName: string;
  folder?: string;
}) => {
  try {
    return JSON.parse(fs.readFileSync(getFilePath(fileName, folder), 'utf-8'));
  } catch (e) {
    //
  }
};
