import * as fs from 'fs';
import {getFilePath} from './get-file-path';

export const saveFile = ({
  fileName,
  folder,
  content,
}: {
  fileName: string;
  folder?: string;
  content: Record<string, unknown>;
}): void => {
  fs.writeFileSync(getFilePath(fileName, folder), JSON.stringify(content, null, '  '), {flag: 'w'});
};
