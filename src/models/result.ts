export interface Result<PositionsEnum extends number> {
  id: number;
  weekId: string;
  teamId: string;

  fantasyPoints: number;
  fantasyPointsTourByTour: number[];

  projectedPoints: number;
  perfectPerformance: number;

  ratingValue: number;
  ratingValueTourByTour: number[];

  fairSchedule: 1 | 0;
  fairScheduleTourByTour: number[];

  record: 1 | 0;
  recordTourByTour: number[];

  positionFantasyPoints: Record<PositionsEnum, {
    value: number;
    count: number;
  }>;
  positionFantasyPointsTourByTour: Record<PositionsEnum, {
    value: number;
    count: number;
  }[]>;
}
