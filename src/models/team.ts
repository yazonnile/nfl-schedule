export interface Team {
  id: number;
  abbrev: string;
  location: string;
  logo: string;
  name: string;
  nickname: string;
}
