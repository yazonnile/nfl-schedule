import {Result} from '@models/result';
import {Team} from '@models/team';

export type Provider<SourceType, PositionsEnum extends number, ResultType = {
  seasonId: number;
  teams: Record<string, Team>;
  weeks: Record<string, Record<string, Result<PositionsEnum>>>;
}> = {
  updateSource: (leagueId: number) => Promise<void>;
  getSource: (leagueId: number) => Promise<SourceType>;
  transform: (leagueId: number, source: SourceType) => ResultType;
}
