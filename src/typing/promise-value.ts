export type PromiseValue<Type> = Type extends Promise<infer Value> ? Value : Type;
