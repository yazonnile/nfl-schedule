import {espnNflProvider} from '@providers/espn/nfl/provider';

(async () => {
  const leagueId = 127345224;
  console.log(espnNflProvider);
  espnNflProvider.transform(
    leagueId,
    await espnNflProvider.getSource(leagueId),
  )
})();
