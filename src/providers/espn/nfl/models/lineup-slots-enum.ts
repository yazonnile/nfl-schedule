/* eslint-disable no-magic-numbers */
export enum LineupSlotsEnum {
  QB = 0,
  RB = 2,
  WR = 4,
  TE = 6,
  OP = 7,
  FLEX = 23,
  BENCH = 20,
  IR = 21,
}
/* eslint-enable no-magic-numbers */
