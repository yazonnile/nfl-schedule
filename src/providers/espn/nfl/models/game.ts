import {GameTeamResult} from './game-team-result';

export interface Game {
  id: number;
  winner: 'AWAY' | 'HOME';
  matchupPeriodId: number;
  away: GameTeamResult;
  home: GameTeamResult;
}
