import {Result as MResult} from '@models/result';
import {PositionsEnum} from './positions-enum';

export type Result = MResult<PositionsEnum>;
