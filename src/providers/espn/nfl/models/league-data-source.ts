import {Team} from '@models/team';
import {LineupSlotsEnum} from './lineup-slots-enum';

export interface EspnNflLeagueDataSource {
  seasonId: number;
  teams: Team[];
  settings: {
    scheduleSettings: {
      matchupPeriodCount: number;
    };
    rosterSettings: {
      lineupSlotCounts: Record<LineupSlotsEnum, number>;
    }
  }
}
