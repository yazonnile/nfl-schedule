/* eslint-disable no-magic-numbers */
export enum PositionsEnum {
  QB = 1,
  RB = 2,
  WR = 3,
  TE = 4,
}
/* eslint-enable no-magic-numbers */
