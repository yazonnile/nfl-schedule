import {PlayerEntry} from './player-entry';

export interface GameTeamResult {
  adjustment: number;
  teamId: number;
  totalPoints: number;

  // all roster scoring
  rosterForCurrentScoringPeriod: {
    entries: PlayerEntry[];
  };

  // matchup roster scoring
  rosterForMatchupPeriod: {
    entries: PlayerEntry[];
  };
}
