import {LineupSlotsEnum} from './lineup-slots-enum';
import {PlayerStats} from './player-stats';
import {PositionsEnum} from './positions-enum';

export type PlayerEntry = {
  lineupSlotId: LineupSlotsEnum;
  playerId: number;
  playerPoolEntry: {
    appliedStatTotal: number,
    player: {
      defaultPositionId: PositionsEnum;
      stats: PlayerStats;
    };
  };
};
