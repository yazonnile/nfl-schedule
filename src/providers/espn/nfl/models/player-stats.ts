export type PlayerStats = Readonly<{
  appliedTotal: number;
  statSourceId: 0 | 1,
}[]>;
