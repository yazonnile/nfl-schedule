import {Game} from './game';

export interface EspnNflWeekDataSource {
  weekId: number;
  schedule: Game[];
}
