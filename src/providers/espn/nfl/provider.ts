import {isFileExists} from '@lib/is-file-exists';
import {openFile} from '@lib/open-file';
import {saveFile} from '@lib/save-file';
import {Team} from '@models/team';
import {Provider} from '@typing/provider';
import {loadLeague} from './api/load-league';
import {loadWeek} from './api/load-week';
import {calculateWeekRate} from './lib/calculate-week-rate';
import {getTeamWeekResult} from './lib/get-team-week-result';
import {Game} from './models/game';
import {Result} from './models/result';
import {EspnNflLeagueDataSource} from './models/league-data-source';
import {EspnNflWeekDataSource} from './models/week-data-source';
import {calculateTourByTourValues} from './lib/calculate-tour-by-tour-values';
import {calculateWeekFairSchedule} from './lib/calculate-week-fair-schedule';
import {PositionsEnum} from './models/positions-enum';
import {calculateTourByTourPositionFantasyPoints} from './lib/calculate-tour-by-tour-position-fantasy-points';

export const folder = './src/providers/espn/nfl';
export const sourceFileName = (leagueId: number) => `espn-nfl-${leagueId}-source.json`;
export const dataFileName = (leagueId: number) => `espn-nfl-${leagueId}-data.json`;

export const espnNflProvider: Provider<
  EspnNflLeagueDataSource & {
    schedule: Record<number, Game[]>;
  },
  PositionsEnum
> = {
  async updateSource(leagueId) {
    const {teams, seasonId, settings} = await loadLeague<EspnNflLeagueDataSource>(leagueId);
    const {matchupPeriodCount: numOfWeeks} = settings.scheduleSettings;

    const weeks: Record<number, Game[]> = {};

    for(let i = 1; i <= numOfWeeks; i++) {
      const {schedule} = await loadWeek<EspnNflWeekDataSource>(leagueId, i);
      weeks[i] = schedule.filter(m => m.matchupPeriodId === i);
    }

    saveFile({
      fileName: sourceFileName(leagueId),
      folder,
      content: {seasonId, schedule: weeks, teams, settings},
    });
  },

  async getSource(leagueId) {
    const params = {
      folder,
      fileName: sourceFileName(leagueId),
    };

    if (!isFileExists(params)) {
      await this.updateSource(leagueId);
    }

    return openFile(params);
  },

  transform(leagueId, {teams: teamsData, schedule: weeks, seasonId, settings}) {
    const result: Record<string, Record<string, Result>> = {};
    const teams: Record<string, Team> = {};
    const weeksMatches: Record<
      string,
      {homeTeamId: Team['id'], awayTeamId: Team['id'], gameId: Game['id']}[]
    > = {};

    for (const team of teamsData) {
      teams[team.id] = {
        id: team.id,
        abbrev: team.abbrev,
        location: team.location,
        logo: team.logo,
        name: team.name,
        nickname: team.nickname,
      };
    }

    for (const [weekId, games] of Object.entries(weeks)) {
      if (!result[weekId]) {
        result[weekId] = {};
      }

      if (!weeksMatches[weekId]) {
        weeksMatches[weekId] = [];
      }

      for (const {id, away, home, winner} of games) {
        if (home) {
          result[weekId][home.teamId.toString()] = {
            id,
            weekId,
            teamId: home.teamId.toString(),
            record: winner === 'HOME' ? 1 : 0,
            ...getTeamWeekResult(home, settings),
          };
        }

        if (away) {
          result[weekId][away.teamId.toString()] = {
            id,
            weekId,
            teamId: away.teamId.toString(),
            record: winner === 'AWAY' ? 1 : 0,
            ...getTeamWeekResult(away, settings),
          };
        }

        if (home && away) {
          weeksMatches[weekId].push({
            gameId: id,
            homeTeamId: home.teamId,
            awayTeamId: away.teamId,
          });
        }
      }

      calculateWeekRate(result[weekId], teamsData.length);
      calculateWeekFairSchedule(result[weekId], teamsData.length);
    }

    calculateTourByTourPositionFantasyPoints(result);
    calculateTourByTourValues(result);

    const transformationData = {
      seasonId,
      teams,
      weeks: result,
      weeksMatches,
    };

    saveFile({
      fileName: dataFileName(leagueId),
      folder,
      content: transformationData,
    });

    return transformationData;
  },
};
