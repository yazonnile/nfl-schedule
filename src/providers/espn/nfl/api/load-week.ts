import fetch from 'node-fetch';

export const loadWeek = async <WeekDataType>(leagueId: number, weekId: number): Promise<WeekDataType> => {
  const response = await fetch(`https://fantasy.espn.com/apis/v3/games/ffl/seasons/2022/segments/0/leagues/${leagueId}?view=mMatchup&view=mMatchupScore&scoringPeriodId=${weekId}`);

  return await response.json() as WeekDataType;
};
