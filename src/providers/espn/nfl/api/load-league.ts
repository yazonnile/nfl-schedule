import fetch from 'node-fetch';

export const loadLeague = async <LeagueDataType>(leagueId: number): Promise<LeagueDataType> => {
  const response = await fetch(`https://fantasy.espn.com/apis/v3/games/ffl/seasons/2022/segments/0/leagues/${leagueId}?view=mSettings&view=mTeam&view=modular&view=mNav`);

  return await response.json() as LeagueDataType;
};
