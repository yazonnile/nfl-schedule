import {GameTeamResult} from '../models/game-team-result';
import {LineupSlotsEnum} from '../models/lineup-slots-enum';
import {PositionsEnum} from '../models/positions-enum';

export const getPerfectTeamPerformance = (
  teamResult: GameTeamResult,
  playersPerSlot: Record<LineupSlotsEnum, number>,
): number => {
  const availablePlayers = teamResult.rosterForCurrentScoringPeriod.entries.filter(it => it.lineupSlotId !== LineupSlotsEnum.IR);
  const result = availablePlayers.reduce((sum, currentPlayer) => {
    sum[currentPlayer.playerPoolEntry.player.defaultPositionId].push(currentPlayer.playerPoolEntry.appliedStatTotal);

    return sum;
  }, {
    [PositionsEnum.QB]: [],
    [PositionsEnum.RB]: [],
    [PositionsEnum.WR]: [],
    [PositionsEnum.TE]: [],
  } as Record<PositionsEnum, number[]>);

  // sort each position results by FPTS
  for (const players of Object.values(result)) {
    players.sort((p1, p2) => {
      return p1 < p2 ? 1 : -1;
    });
  }

  let perfectPerformance = 0;

  // calculate slot positions
  for (const it of ['QB', 'RB', 'WR', 'TE'] as const) {
    perfectPerformance += result[PositionsEnum[it]].splice(0, playersPerSlot[LineupSlotsEnum[it]]).filter(Boolean)
      .reduce((sum, current) => sum + current, 0);
  }

  const flexPlayersCount = playersPerSlot[LineupSlotsEnum.FLEX];
  const opPlayersCount = playersPerSlot[LineupSlotsEnum.OP];
  const flexAndOpPlayersCount = flexPlayersCount + opPlayersCount;

  const resultsFlexAndOP = [
    ...result[PositionsEnum.RB].splice(0, flexAndOpPlayersCount),
    ...result[PositionsEnum.WR].splice(0, flexAndOpPlayersCount),
    ...result[PositionsEnum.TE].splice(0, flexAndOpPlayersCount),
  ].filter(Boolean).sort((p1, p2) => p1 > p2 ? -1 : 1);

  // calculate flex
  perfectPerformance += resultsFlexAndOP
    .splice(0, flexPlayersCount)
    .reduce((sum, current) => sum + current, 0);

  // calculate OP
  perfectPerformance += [
    ...result[PositionsEnum.QB].splice(0, opPlayersCount),
    ...resultsFlexAndOP,
  ]
    .filter(Boolean)
    .sort((p1, p2) => p1 > p2 ? -1 : 1)
    .splice(0, opPlayersCount)
    .reduce((sum, current) => sum + current, 0);

  return perfectPerformance;
};
