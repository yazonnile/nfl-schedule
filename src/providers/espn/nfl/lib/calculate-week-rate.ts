import {Result} from '../models/result';

export const calculateWeekRate = (weekResult: Record<string, Result>, teamsCount: number) => {
  const fantasyPointsArray = Object.values(weekResult).map(({fantasyPoints}) => fantasyPoints);
  const minFantasyPoints = Math.min(...fantasyPointsArray);
  const maxFantasyPoints = Math.max(...fantasyPointsArray);
  const maxPossibleRateForWeek = 10;
  const minMaxDiff = (maxFantasyPoints - minFantasyPoints) / maxPossibleRateForWeek;
  const sortedResultsByFantasyPoints = [...Object.values(weekResult)].sort((a, b) => {
    return a.fantasyPoints > b.fantasyPoints ? -1 : a.fantasyPoints < b.fantasyPoints ? 1 : 0;
  });

  for (const [index, {teamId, fantasyPoints}] of Object.entries(sortedResultsByFantasyPoints)) {
    const rate1 = maxPossibleRateForWeek * (teamsCount - parseFloat(index) - 1) / (teamsCount - 1);
    const rate2 = (fantasyPoints - minFantasyPoints) / minMaxDiff;

    weekResult[teamId].ratingValue = (rate1 + rate2) / 2;
  }
};
