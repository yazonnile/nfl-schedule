import {GameTeamResult} from '../models/game-team-result';
import {PositionsEnum} from '../models/positions-enum';

export const getPositionFantasyPoints = (teamResult: GameTeamResult): Record<PositionsEnum, {
  count: number;
  value: number;
}> => {
  return teamResult.rosterForMatchupPeriod.entries.reduce((sum, currentPlayer) => {
    sum[currentPlayer.playerPoolEntry.player.defaultPositionId].count++;
    sum[currentPlayer.playerPoolEntry.player.defaultPositionId].value += currentPlayer.playerPoolEntry.appliedStatTotal;

    return sum;
  }, {
    [PositionsEnum.QB]: {count: 0, value: 0},
    [PositionsEnum.RB]: {count: 0, value: 0},
    [PositionsEnum.WR]: {count: 0, value: 0},
    [PositionsEnum.TE]: {count: 0, value: 0},
  } as Record<PositionsEnum, {
    count: number;
    value: number;
  }>);
};
