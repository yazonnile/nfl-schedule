export const toFixedNumber = (
  num: number, precision = 2,
): number => {
  return Number(num.toFixed(precision));
};
