import {PositionsEnum} from '../models/positions-enum';
import {Result} from '../models/result';
import {transformWeeksToTeamsResults} from './transform-weeks-to-teams-results-array';

export const calculateTourByTourPositionFantasyPoints = (weeks: Record<string, Record<string, Result>>): void => {
  const teamsResults = transformWeeksToTeamsResults(weeks);

  for (const [weekId, teamsWeekResults] of Object.entries(weeks)) {
    for (const [teamId, teamWeekResult] of Object.entries(teamsWeekResults)) {
      const teamFromStartToWeeksValues = teamsResults[teamId]
        .slice(0, Number(weekId))
        .map(it => it.positionFantasyPoints);
      const positions = [
        PositionsEnum.QB,
        PositionsEnum.RB,
        PositionsEnum.TE,
        PositionsEnum.WR,
      ] as const;

      for (const position of positions) {
        teamWeekResult.positionFantasyPointsTourByTour[position] = teamFromStartToWeeksValues.map(it => it[position]);
      }
    }
  }
};
