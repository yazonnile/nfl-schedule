import {Result} from '../models/result';
import {transformWeeksToTeamsResults} from './transform-weeks-to-teams-results-array';

const tourByTourParams: Array<
  keyof Pick<Result, 'fantasyPoints' | 'ratingValue' | 'fairSchedule' | 'record'>
> = [
  'fantasyPoints',
  'ratingValue',
  'fairSchedule',
  'record',
];

export const calculateTourByTourValues = (weeks: Record<string, Record<string, Result>>): void => {
  const teamsResults = transformWeeksToTeamsResults(weeks);

  for (const [weekId, teamWeekResults] of Object.entries(weeks)) {
    for (const [teamId, teamWeekResult] of Object.entries(teamWeekResults)) {
      for (const param of tourByTourParams) {
        const paramValue = teamsResults[teamId]
          .slice(0, Number(weekId))
          .map(it => it[param]);

        teamWeekResult[`${param}TourByTour`] = paramValue;
      }
    }
  }
};
