import {PlayerStats} from '../models/player-stats';

export const getPlayerPrediction = (stats: PlayerStats): number => {
  return stats.find(s => s.statSourceId === 1)?.appliedTotal || 0;
};
