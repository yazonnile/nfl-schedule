import {Result} from '../models/result';
import {GameTeamResult} from '../models/game-team-result';
import {EspnNflLeagueDataSource} from '../models/league-data-source';
import {getPerfectTeamPerformance} from './get-perfect-team-performance';
import {getPlayerPrediction} from './get-player-prediction';
import {isPlayerInMatchRoster} from './is-player-in-match-roster';
import {PositionsEnum} from '../models/positions-enum';
import {getPositionFantasyPoints} from './get-position-fantasy-points';

export const getTeamWeekResult = (
  teamResult: GameTeamResult,
  settings: EspnNflLeagueDataSource['settings'],
): Omit<Result, 'id' | 'weekId' | 'teamId' | 'record'> => {
  const fantasyPoints = teamResult.totalPoints;
  const projectedPoints = teamResult.rosterForCurrentScoringPeriod.entries
    .filter(isPlayerInMatchRoster)
    .reduce((result, {playerPoolEntry}) => {
      return result + getPlayerPrediction(playerPoolEntry.player.stats);
    }, 0);

  const perfectPerformance = getPerfectTeamPerformance(
    teamResult,
    settings.rosterSettings.lineupSlotCounts,
  );

  const positionFantasyPoints = getPositionFantasyPoints(teamResult);

  return {
    fantasyPoints,
    projectedPoints,
    perfectPerformance,
    positionFantasyPoints,

    // calculated later in calculateAdvancedMetrics
    ratingValue: 0,

    // calculated later in calculateWeekFairSchedule
    fairSchedule: 0,

    // updates later in calculateTourByTourValues
    fantasyPointsTourByTour: [],
    ratingValueTourByTour: [],
    fairScheduleTourByTour: [],
    recordTourByTour: [],
    positionFantasyPointsTourByTour: {
      [PositionsEnum.QB]: [],
      [PositionsEnum.RB]: [],
      [PositionsEnum.TE]: [],
      [PositionsEnum.WR]: [],
    },
  };
};
