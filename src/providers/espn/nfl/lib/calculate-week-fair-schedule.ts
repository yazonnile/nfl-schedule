import {Result} from '../models/result';

export const calculateWeekFairSchedule = (weekResult: Record<string, Result>, teamsCount: number) => {
  const sortedResultsByRate = [...Object.values(weekResult)].sort((a, b) => {
    return a.ratingValue > b.ratingValue ? -1 : a.ratingValue < b.ratingValue ? 1 : 0;
  });

  const halfTeamsCount = teamsCount / 2;
  const weekRateMedian = (sortedResultsByRate[halfTeamsCount - 1].ratingValue + sortedResultsByRate[halfTeamsCount].ratingValue) / 2;

  for (const {teamId, ratingValue} of Object.values(sortedResultsByRate)) {
    weekResult[teamId].fairSchedule = ratingValue > weekRateMedian ? 1 : 0;
  }
};
