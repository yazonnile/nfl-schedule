import {LineupSlotsEnum} from '../models/lineup-slots-enum';
import {PlayerEntry} from '../models/player-entry';

export const isPlayerInMatchRoster = ({lineupSlotId}: PlayerEntry) => {
  return LineupSlotsEnum.BENCH !== lineupSlotId && LineupSlotsEnum.IR !== lineupSlotId;
};
