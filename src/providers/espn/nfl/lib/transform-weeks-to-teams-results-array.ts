import {Result} from '../models/result';

export const transformWeeksToTeamsResults = (weeks: Record<string, Record<string, Result>>): Record<string, Result[]> => {
  const allResultsArray = Object.values(weeks)
    .map(it => Object.values(it))
    .flat();

  const teamsResults: Record<string, Result[]> = {};

  for (const teamWeekResult of allResultsArray) {
    if (!teamsResults[teamWeekResult.teamId]) {
      teamsResults[teamWeekResult.teamId] = [];
    }

    teamsResults[teamWeekResult.teamId].push(teamWeekResult);
  }

  for (const teamResults of Object.values(teamsResults)) {
    teamResults.sort((a, b) => Number(a.weekId) < Number(b.weekId) ? -1 : 1);
  }

  return teamsResults;
};
