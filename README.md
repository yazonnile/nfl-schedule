https://trpc.io/docs/quickstart

# now
- db scripts
  - create a DB based on schema
  - snapshot/restore
  - tear down
  - create migration file/revert file
- ORM


# next
- install process (configs)
- dev/prod config
- lefthook (pre-push, pre-commit)
- client styles - svelte styles? https://vanilla-extract.style/ ? stylelint? css variables
- vite
- SSR
- github CI
- docker?


/*
DB shared module should

db connect
db clear
migration script
db migrate

db methods
  select
  join
  delete
  update
  insert
  where
  limit
  count
  OTHER POSTGRES FEATURES!!!

Router should be in shared
*/


createdb
psql -f ./db-install.sql
